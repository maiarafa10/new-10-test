About test-new10
=================

## Libraries used
- Java 8
- Spring Boot 2

# Installation

## Prerequisites 
- Installed JDK 8

## Installation steps
1. clone project from GitLab: `git clone https://gitlab.com/maiarafa10/new-10-test.git`
2. build with the following maven command: `mvn clean install`

## Running App 
- Create a S3 Bucket Aws Console and give it full Public Access
- set up com.new10.constant.Constant.java 
`UCKET_NAME = "rafa.test";
 CLIENT_REGION = "eu-west-3";` 
 - Upload Camt Xml file
 
 
### AWS 
- Create new Lambda function 
- Rnuntime Java 8 
- Handler: com.new10.InputProcessor::handleRequest
- Upload `test-new10-0.0.1-SNAPSHOT.jar` 
- Create a simple test sending a String (File name)

### Local 
- Call Method processFile sending a String (File name)


### Outcome 
- Check S3 Bucket file: timestamp_netbalance.json
`{  
     "daysInDebti":18,
     "summaryByMonths":[  
        {  
           "month":1,
           "year":2018,
           "amt":19820.0
        }   
     ]
  }`


## Build artifact
`test-new10-0.0.1-SNAPSHOT.jar`

# Developers
@maiarafa10