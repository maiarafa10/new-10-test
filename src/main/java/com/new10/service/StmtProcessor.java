package com.new10.service;


import com.new10.dto.SummaryByDay;
import com.new10.dto.SummaryByMonth;
import com.new10.pojo.Stmt;

import java.time.LocalDate;
import java.util.List;

public interface StmtProcessor {



    /**
     * Returns net balance by year / month / day
     *
     * @param stmt bank statement to be evaluated
     * @return list <SummaryByDay>
     */
    List<SummaryByDay> getNetBalanceSummaryByDay(Stmt stmt);


    /**
     * Returns no of days in debt during last 3 months
     *
     * @param stmt bank statement to be evaluated
     * @return no of days in debt
     */
    Long getDaysDebitLastThreeMonths(List<SummaryByDay> summaryByDayList, LocalDate endOfStmt);

    /**
     * Returns net balance by year / month
     *
     * @param stmt bank statement to be evaluated
     * @return list <SummaryByDay>
     */
    List<SummaryByMonth> getNetBalanceSummaryByMonth(List<SummaryByDay> summaryByDayList);

}
