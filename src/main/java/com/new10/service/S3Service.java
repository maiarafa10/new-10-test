package com.new10.service;

import com.amazonaws.services.s3.AmazonS3;
import com.new10.pojo.Document;

import java.io.File;

public interface S3Service {
    /**
     * Returns XML Pojo ile from S3 Bucket
     *
     * @param filename string
     * @return InputStream
     */
    Document getFile(String filename);

    /**
     * Inserts a new file and returns filename
     *
     * @param File file
     * @return String
     */
    String putFile(File file);

    AmazonS3 getInstance();
}
