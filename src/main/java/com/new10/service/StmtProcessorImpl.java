
package com.new10.service;



import com.new10.dto.SummaryByDay;
import com.new10.dto.SummaryByMonth;
import com.new10.pojo.NtryItem;
import com.new10.pojo.Stmt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;



@Service
public class StmtProcessorImpl implements StmtProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(StmtProcessorImpl.class);


    private Function<NtryItem, List<Object>> keyYearMonthDay = ntryItem ->
            Arrays.asList(ntryItem.getValDt().getDt().getYear(),
                                  ntryItem.getValDt().getDt().getMonthValue(),
                                  ntryItem.getValDt().getDt().getDayOfMonth());

    private Function<SummaryByDay, List<Object>> keyYearMonth = item ->
            Arrays.asList(item.getDay().getYear(),
                    item.getDay().getMonthValue());


    public List<SummaryByDay> getNetBalanceSummaryByDay(Stmt stmt){

        LOGGER.info("getNetBalanceSummaryByDay  - START");

        LOGGER.info("Create grouped map by Year, Month, Day");
        Map<Object, List<NtryItem>> itemMap =
                     stmt.getNtry().stream().collect(Collectors.groupingBy(keyYearMonthDay, Collectors.toList()));

        LOGGER.info("Map created  [itemMap:{}]", itemMap);
        List<SummaryByDay> newSummaryByDay =  new ArrayList<>();

        itemMap.forEach((key,values)-> {
            Double credit = values.stream().filter(item -> item.getCdtDbtInd().contentEquals("CRDT"))
                                         .mapToDouble(elem -> elem.getAmt().getValue()).sum();
            LOGGER.info("SUM [Credit: {}] for the [day: {}]   ", credit, values.get(0).getValDt().getDt() );
            Double debit = values.stream().filter(item -> item.getCdtDbtInd().contentEquals("DBIT"))
                    .mapToDouble(elem -> elem.getAmt().getValue()).sum();
            LOGGER.info("SUM [Debiy: {}] for the [day: {}]   ", debit, values.get(0).getValDt().getDt() );
            newSummaryByDay.add(new SummaryByDay(values.get(0).getValDt().getDt(), credit - debit));

        });
        LOGGER.info("getNetBalanceSummaryByDay  - END");
        return newSummaryByDay;
    }

    public Long getDaysDebitLastThreeMonths(List<SummaryByDay> summaryByDayList, LocalDate endOfStmt){
        LOGGER.info("getDaysDebitLastThreeMonths  - START");
        return summaryByDayList.stream().filter(item -> item.getDay().isAfter(endOfStmt.minusMonths(3)))
                                   .filter(item -> item.getNet() < 0).count();
    }

    public List<SummaryByMonth> getNetBalanceSummaryByMonth(List<SummaryByDay> summaryByDayList){
        LOGGER.info("getNetBalanceSummaryByMonth  - START");

        LOGGER.info("Create grouped map by Year, Month");
        Map<Object, List<SummaryByDay>> itemMap =
                summaryByDayList.stream().collect(Collectors.groupingBy(keyYearMonth, Collectors.toList()));

        List<SummaryByMonth> summaryByMonthArrayList =  new ArrayList<>();
        itemMap.forEach((key,values)-> {
            Double amt = values.stream().mapToDouble(elem -> elem.getNet()).sum();
            LOGGER.info("Calc [Amount: {}] for the [Month: {}] - [Year {}]   ", amt,
                    values.get(0).getDay().getMonthValue(), values.get(0).getDay().getYear());

            summaryByMonthArrayList.add(new SummaryByMonth(values.get(0).getDay().getMonthValue(), values.get(0).getDay().getYear(), amt));

        });
        LOGGER.info("getNetBalanceSummaryByDay  - END");
        return summaryByMonthArrayList;

    }

}
