package com.new10.service;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.new10.InputProcessor;
import com.new10.pojo.Document;
import com.new10.constant.Contants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;


public class S3ServiceImpl implements S3Service{

    private static final Logger LOGGER = LoggerFactory.getLogger(S3ServiceImpl.class);
    public AmazonS3 getInstance(){
        return AmazonS3ClientBuilder.standard()
                .withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
                .withRegion(Contants.CLIENT_REGION)
                .build();
    }


    public String putFile(File file){
        LOGGER.info("putFile - START");
        String filename = System.currentTimeMillis() + "_netbalance";
        getInstance().putObject(Contants.BUCKET_NAME, filename, file);
        LOGGER.info("putFile - END");
        return filename;
    }

    public Document getFile(String filename){
         try{
              InputStream s3stream = getInstance().getObject(Contants.BUCKET_NAME, filename).getObjectContent();
              JAXBContext jaxbContext = JAXBContext.newInstance(Document.class);
              Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
              Document stmtPojo = (Document) jaxbUnmarshaller.unmarshal(s3stream);
             return stmtPojo;
         } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }
}
