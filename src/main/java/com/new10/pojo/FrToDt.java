package com.new10.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@XmlRootElement(name = "FrToDt")
public class FrToDt {

	private LocalDate toDtTm;

	private LocalDate frDtTm;

	@XmlElement(name = "ToDtTm")
	@XmlJavaTypeAdapter(com.new10.util.LocalDateAdapter.class)
	public LocalDate getToDtTm() {
		return toDtTm;
	}

	public void setToDtTm(LocalDate toDtTm) {
		this.toDtTm = toDtTm;
	}

	@XmlElement(name = "FrDtTm")
	@XmlJavaTypeAdapter(com.new10.util.LocalDateAdapter.class)
	public LocalDate getFrDtTm() {
		return frDtTm;
	}

	public void setFrDtTm(LocalDate frDtTm) {
		this.frDtTm = frDtTm;
	}

}