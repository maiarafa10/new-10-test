package com.new10.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;


@XmlRootElement(name = "ValDt")
public class ValDt {

	private LocalDate dt;

	@XmlElement(name = "Dt")
	@XmlJavaTypeAdapter(com.new10.util.LocalDateAdapter.class)
	public LocalDate getDt() {
		return dt;
	}

	public void setDt(LocalDate dt) {
		this.dt = dt;
	}
}