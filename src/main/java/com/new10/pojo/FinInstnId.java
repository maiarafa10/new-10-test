package com.new10.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinInstnId{

	@JsonProperty("BIC")
	private String bIC;

	public void setBIC(String bIC){
		this.bIC = bIC;
	}

	public String getBIC(){
		return bIC;
	}
}