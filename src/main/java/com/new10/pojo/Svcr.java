package com.new10.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Svcr{

	@JsonProperty("FinInstnId")
	private FinInstnId finInstnId;

	public void setFinInstnId(FinInstnId finInstnId){
		this.finInstnId = finInstnId;
	}

	public FinInstnId getFinInstnId(){
		return finInstnId;
	}
}