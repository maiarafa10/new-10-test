package com.new10;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.new10.dto.NetBalanceResponse;
import com.new10.dto.SummaryByDay;
import com.new10.dto.SummaryByMonth;
import com.new10.pojo.Stmt;
import com.new10.service.S3Service;
import com.new10.service.S3ServiceImpl;
import com.new10.service.StmtProcessor;
import com.new10.service.StmtProcessorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class InputProcessor implements RequestHandler<String, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InputProcessor.class);


    public String handleRequest(String input,  Context context)   {
      return processFile(input);
    }


    public String  processFile(String input)  {

        S3Service s3Service = new S3ServiceImpl();


        StmtProcessor stmtProcessor = new StmtProcessorImpl();

        NetBalanceResponse netBalanceResponse;

        Stmt stmt = s3Service.getFile(input).getBkToCstmrStmt().getStmt();

        List<SummaryByDay> summaryByDays = stmtProcessor.getNetBalanceSummaryByDay(stmt);
        List<SummaryByMonth> summaryByMonths = stmtProcessor.getNetBalanceSummaryByMonth(summaryByDays);
        long daysInDebit = stmtProcessor.getDaysDebitLastThreeMonths(summaryByDays, stmt.getFrToDt().getFrDtTm());

        netBalanceResponse = new NetBalanceResponse(daysInDebit, summaryByMonths);

        ObjectMapper mapper = new ObjectMapper();

        String tempDir = System.getProperty("java.io.tmpdir");
        LOGGER.info("create [tempDir: {}] ", tempDir);
        try {
            mapper.writeValue(new File(tempDir+"/netBalanceResponse.json"), netBalanceResponse );
            s3Service.putFile(new File(tempDir+"/netBalanceResponse.json"));
            LOGGER.info("create [tempDir: {}] ", tempDir);
            return tempDir;
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("Error creating [file: {}]", e.getMessage());
            return e.getMessage();
        }
    }

}
