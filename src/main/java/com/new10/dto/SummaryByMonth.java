package com.new10.dto;

public class SummaryByMonth {
    private int month;
    private int year;
    private double amt;

    public SummaryByMonth() {
    }

    public SummaryByMonth(int month, int year, double amt) {
        this.month = month;
        this.year = year;
        this.amt = amt;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getAmt() {
        return amt;
    }

    public void setAmt(double amt) {
        this.amt = amt;
    }
}
