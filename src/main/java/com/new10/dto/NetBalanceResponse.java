package com.new10.dto;

import java.util.List;

public class NetBalanceResponse {
    private long daysInDebt;
    private List<SummaryByMonth> summaryByMonths;

    public NetBalanceResponse(long daysInDebti, List<SummaryByMonth> summaryByMonths) {
        this.daysInDebt = daysInDebti;
        this.summaryByMonths = summaryByMonths;
    }

    public long getDaysInDebti() {
        return daysInDebt;
    }

    public void setDaysInDebti(long daysInDebti) {
        this.daysInDebt = daysInDebti;
    }

    public List<SummaryByMonth> getSummaryByMonths() {
        return summaryByMonths;
    }

    public void setSummaryByMonths(List<SummaryByMonth> summaryByMonths) {
        this.summaryByMonths = summaryByMonths;
    }
}
