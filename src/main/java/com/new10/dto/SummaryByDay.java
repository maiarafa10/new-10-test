package com.new10.dto;


import java.time.LocalDate;

public class SummaryByDay {

    private LocalDate day;
    private Double net;

    public SummaryByDay() {
    }

    public SummaryByDay(LocalDate day, Double net) {
        this.day = day;
        this.net = net;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public Double getNet() {
        return net;
    }

    public void setNet(Double net) {
        this.net = net;
    }
}
