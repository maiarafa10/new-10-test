package com.new10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestNew10Application {

	public static void main(String[] args) {
		SpringApplication.run(TestNew10Application.class, args);
	}
}
